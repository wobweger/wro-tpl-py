#----------------------------------------------------------------------------
# Name:         ldwEntry.py
# Purpose:      package entry
#               
# Author:       Walter Obweger
#
# Created:      20220219
# CVS-ID:       $Id$
# Copyright:    (c) 2022 by Siemens AG
# Licence:      MIT
#----------------------------------------------------------------------------

def funcAdd(i,j):
    """ simple added

    Args:
        i: first value.
        j: second value.
    Returns:
        value : result
    """
    return i+j

def funcCmp(i,j):
    """ simple added

    Args:
        i: first value.
        j: second value.
    Returns:
        result of compare.

        -1:i<j

        0: i==j

        1: i>j
    """
    if i<j:
        return -1
    elif i>j:
        return 1
    else:
        return 0

def main(args=None):
    """python app entry point
    """
    # +++++ beg:
    # ----- end:
    # +++++ beg:init
    iVerbose=5
    iRet=0
    # ----- end:init
    # +++++ beg:add arguments
    try:
        if len(args>2):
            iVal=0
            for val in args[1:]:
                iVal=funcAdd(iVal,int(val))
            print('\nresult:%d'%(iVal))
            print('\nend processing')
    except:
        iRet=-1
    # ----- end:add arguments
    # +++++ beg:finished
    print('   iRet:%r'%(iRet))
    # ----- end:finished
    return iRet

if __name__=='__main__':
    import sys
    main(sys.argv)
