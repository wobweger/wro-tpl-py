#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      module initialization
#               
# Author:       Walter Obweger
#
# Created:      20220219
# CVS-ID:       $Id$
# Copyright:    (c) 2022 by Siemens AG
# Licence:      MIT
#----------------------------------------------------------------------------

__version__='0.0.1'
