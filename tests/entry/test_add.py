#----------------------------------------------------------------------------
# Name:         test_add.py
# Purpose:      test adder function
#               
# Author:       Walter Obweger
#
# Created:      20220219
# CVS-ID:       $Id$
# Copyright:    (c) 2022 by Siemens AG
# Licence:      MIT
#----------------------------------------------------------------------------

import ldwPkg.ldwEntry

def test_addInt():
    assert ldwPkg.ldwEntry.funcAdd(1,1)==2

def test_addFloat():
    assert ldwPkg.ldwEntry.funcAdd(1.0,1.2)==2.2
