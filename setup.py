#!/usr/bin/env python
#----------------------------------------------------------------------------
# Name:         setup.py
# Purpose:      setup definition
#               
# Author:       Walter Obweger
#
# Created:      20220219
# CVS-ID:       $Id$
# Copyright:    (c) 2022 by Siemens AG
# Licence:      MIT
#----------------------------------------------------------------------------

import io
import os
import re
import sys
from setuptools import setup

# +++++ beg:get version number from package init
with open('src/ldwPkg/__init__.py', 'r') as oFile:
    sVersion = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        oFile.read(), re.MULTILINE).group(1)
# ----- end:get version number from package init

# +++++ beg:use readme as description
# Use the README.md content for the long description:
with io.open("src/ldwPkg/README.md", encoding="utf-8") as oFile:
    sDescLong = oFile.read()
# ----- end:use readme as description

setup(
    name='ldwPkg',
    version=sVersion,
    description='short package description',
    long_description=sDescLong,
    long_description_content_type="text/markdown",
    author='Walter Obweger',
    author_email='walter.obweger@gmail.com',
    url='https://gitlab.com/wro/wro-tlp-py',
    project_urls={
        'Documentation': 'https://gitlab.com/wro/wro-tlp-py',
    },
    license='MIT',
    packages=[
        'ldwPkg',
    ],
    package_dir={'':'src'},
    test_suite='tests',
    install_requires=[
        'pandas',
        'pytest',
        ],
    setup_requires=['flake8'],
    keywords="python template",
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Siemens Internal License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Utilities'
    ],
    entry_points={
        'console_scripts': [
            'ldwPkg.lwdEntry = ldwPkg.lwdEntry:main',
        ],
        'gui_scripts': [
        ]
    },
)
